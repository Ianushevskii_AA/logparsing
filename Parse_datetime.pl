#!/usr/bin/perl
use strict;

# parse time and date to array of 4 elements: year, mont, day, seconds.
sub parse_datetime {
    my ($date, $time) = @_;
    my @datetime = ();
    #check date format with regex
    if ($date !~ /(0[1-9]|[12][0-9]|3[01])[- \..](0[1-9]|1[012])[- \..](19|20)\d\d/) {
        return @datetime;
    }
    #check time format with regex
    if ($time !~  /^(([0,1][0-9])|(2[0-3])):[0-5][0-9]:[0-5][0-9]$/) {
        return @datetime;
    }
    #parse date in format DD.MM.YYYY
    my @bufer = split /\./, $date;
    push @datetime, @bufer;
    
    #parse time in format HH:MM:SS
    @bufer = split /\:/, $time;
    
    push @datetime, ($bufer[0] *3600 + $bufer[1] * 60 + $bufer[2]);
    return @datetime;
}
1;
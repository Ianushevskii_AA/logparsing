# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary: simple perl script to parse log files
* Version 1.2.0

### How to run? ###

1. Main script - 'LogParsing.pl', other scripts contain necesary subroutines
2. To run script set as argument path to folder with log files.
   Or set as arguments full pat and name of log files.
   (log files must have extension '.log')

### Contribution guidelines ###

Description of some code parts:
1. Created special hash with resource name as key and array of 4 number elements as value.
      element with number 1 and 3 - time in seconds for start and stop:
            when started start or started stop process - in elements 2 and 3 saving time braces betwen restart time and this event
      element with number 0 and 2 contain status of start and stop:
            status '-1' - resource even no started process of start/stop
            status '0' - resource started process of start/stop, but didn't finished start/stop
            status '1' - resource finished process of start/stop

2. Skript LogParsing.pl checking input console arguments and call subroutine for handle one file as many times as number of log file.
   Also this skript contain global variables of last system restart time and hash of resource statistics (hash description in previous point).

3. Skript Parse_one_file.pl in while loop line by line handle log file and if occur system restart string print in result file all hash, othetwise just add new  information to hash.

4. Also exist file with errors, occurs while parsing. In this file write almost all errors, excepterrors:
   - can't open file;
   - resource event time earlier, than last restart;
   When this 2 error occur, skript stoped.

5. Max time for resource start from last restart - end of next day? otherwice in error log writed timeout.
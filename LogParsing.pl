#!/usr/bin/perl
use strict;

require 'Parse_one_file.pl';
require 'Export_res_stats.pl';

#counter of system restarts
my $Restart_Count = 1;

=coment
hash of resourses states in one system iteration
key - resource name, value - array of 4 elements.
    element with number 1 and 3 - time in seconds for start and stop:
            when started start or started stop process - in elements 2 and 3 saving time braces betwen restart time and this event
      element with number 0 and 2 contain status of start and stop:
            status '-1' - resource even no started process of start/stop
            status '0' - resource started process of start/stop, but didn't finished start/stop
            status '1' - resource finished process of start/stop
=cut
my %Res_Stats;

=coment
array of time with last system restart, that contain:
0 - year
1 - month
2 - day
3 - seconds passed from day start
=cut
my @Last_Restart_Time;

#for checking extension of files
my @extensions = qw(.log);

sub clear_files{
        #clear result and error files
        my($buf_dir_res) = @_;
        open clear, ">", $buf_dir_res."log.res";
        print clear;
        close clear;
        open clear, ">", $buf_dir_res."log.err";
        print clear;
        close clear;
}

#path to dir or file and open file or get all records in dir and in cycle open only files
my $File_Name = "";
#path to dir where save result and error log
my $Dir_Res = "";

#read command line arguments
my $num_args = $#ARGV + 1;

#message, when wrong number of arguments
if ($num_args == 0) {
    print "Wrong number of command line arguments.\n";
    print "Write paths to file or path to folder with logs.\n";
    print "Result save to folder, where placed first log. Result save in 2 files:\n";
    print "log.res  - file with results.\n";
    print "log.err - file with errors, that occur,\n";
    print "while processe collect resultats.\n";
}

use File::Basename;

#for one argument
if ($num_args == 1) {
    $File_Name = $ARGV[0];
    #if argument - path to directory
    if (-d $File_Name) { 
        #add symbol / to end of directory, if haven't
        if ((substr $File_Name, -1) ne "/") {
            $File_Name = $File_Name."/";
        }
        #open directiry for read list of files
        opendir(D, $File_Name) || die "Can't open directory: $!\n";
        #cycle for check all elements in directory
        while (my $file = readdir(D)) {
            #open and parse file if he with extension .log
            if (-f $File_Name.$file) {
                my ($name, $path, $ext) = fileparse($File_Name.$file, @extensions);
                if ($ext eq ".log") {
                    if ($Dir_Res eq "") {
                        $Dir_Res = $path;
                        clear_files($Dir_Res);
                    }
                    $Restart_Count = parse_one_file($File_Name.$file, \%Res_Stats, $Restart_Count, \@Last_Restart_Time, $Dir_Res);
                }
            }
        }
        closedir(D);
    }
    #for case, when one argument is path to file
    elsif (-f $File_Name) {
        my ($name, $path, $ext) = fileparse($File_Name, @extensions);
        if ($ext eq ".log") {
            $Dir_Res = $path;
            clear_files($Dir_Res);
            $Restart_Count = parse_one_file($File_Name, \%Res_Stats, $Restart_Count, \@Last_Restart_Time, $Dir_Res);
        }
    }
    else {
        print "Wrong input argument. Not path to directory or path to file.\n";
    }
}

#if 2 or more arguments - process only files with extension .log, other files skiped
for (my $i = 0; $i < $num_args; $i++) {
    if (-f $ARGV[$i]) {
        my ($name, $path, $ext) = fileparse($ARGV[$i], @extensions);
        if ($ext eq ".log") {
                if ($Dir_Res eq "") {
                    $Dir_Res = $path;
                    clear_files($Dir_Res);
                }
                #parse one file
                $Restart_Count = parse_one_file($ARGV[$i], \%Res_Stats, $Restart_Count, \@Last_Restart_Time, $Dir_Res);
        }
    }
}

#on end of program write in file last information from hash
#open file
open my $wr_res, ">>", $Dir_Res."log.res";
export_and_clear_res_stats(\%Res_Stats, $wr_res);
 #close resource
 close wr_res;
 
print "Parsing of log files complete. Check directory with first log files,\n";
print "for read result documents \"log.res\" and \"log.err\".\n";
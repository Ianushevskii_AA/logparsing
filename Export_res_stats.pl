#!/usr/bin/perl
use strict;

#write hash, geted by reference, in file by filehandle, and then clear hash
sub export_and_clear_res_stats {
    my ($res_stats_ref, $wr_res)= @_;
    #export statistic from hash
    foreach my $res (sort keys %$res_stats_ref) {
        #tresource started start, but not ended
         my $stat = $res_stats_ref -> {$res};
         if (@{$stat}[0] == 0) {
             print $wr_res "$res start didn't end\n";
         }
         elsif (@{$stat}[0] == 1) {
             #resource complete start
             my $word_end = "";
              if (@{$stat}[1] != 1) { $word_end = "s"; }
              print $wr_res "$res started @{$stat}[1] second$word_end\n";
         }
         if (@{$stat}[2] == 0) {
             #resource started stop, but bot ended
             print $wr_res "$res stop didn't end\n";
         }
         elsif (@{$stat}[2] == 1) {
             #resource complete stop
             my $word_end = "";
             if (@{$stat}[3] != 1) { $word_end = "s"; }
             print $wr_res "$res stopped @{$stat}[3] second$word_end\n";
         }
    }
    #clear hash after export statistic
    for (keys %$res_stats_ref) {
        delete $res_stats_ref->{$_};
     }
 }
 1;
#!/usr/bin/perl
    use strict;

require 'Parse_datetime.pl';
require 'Export_res_stats.pl';

#subroutine for compare date of systyem restart and resource state
sub compare_date {
    my ($date_restart_ref, $date_res_ref) = @_;
    
    if (@{$date_restart_ref}[2] != @{$date_res_ref}[2] ||
    @{$date_restart_ref}[1] != @{$date_res_ref}[1] ||
    @{$date_restart_ref}[0] != @{$date_res_ref}[0] &&
    @{$date_restart_ref}[0] + 1 != @{$date_res_ref}[0]) {
        return -1;
    }
    if (@{$date_restart_ref}[0] + 1 == @{$date_res_ref}[0]) {
        return 1;
    }
    return 0;
}

#subroutine for parse one log file.
sub parse_one_file {
    my ($file_name, $res_stats_ref, $restart_count, $last_time_restart_ref, $dir_res) = @_;
  
    #number of file line for error log
    my $file_line = 0;
  
    #open file with log for parse
    open(my $fh, '<', $file_name) or die "Could not open file '$file_name' $!";
    #open file with error loge dor write errors
    open(my $wr_res, ">>", $dir_res."log.res") or die "Could not open file '$dir_res'.log.res $!";
    #open file with result, for write result
    open(wr_err, ">>", $dir_res."log.err") or die "Could not open file '$dir_res.'log.err $!";
  
    #parse of each string in file
    while (my $row = <$fh>) {
        $file_line++;
        chomp $row;
    
        #separate one string of log by spaces
        my @split_log_line=split / /, $row;
    
        if (@split_log_line == 4 && $split_log_line[2] eq "SYSTEM" && $split_log_line[3] eq "START") {
            #save date and time of last restart in array
            my @bufer = parse_datetime($split_log_line[0], $split_log_line[1]);
            #sklip line in file, if wrong date or time format
            if (@bufer != 4) {
                print wr_err "wrong date or time format in file \"$file_name\" on line $file_line\n"; 
                next;
            }
            @{$last_time_restart_ref} = @bufer;
            
            #write resource stats in file and clear hash
            export_and_clear_res_stats($res_stats_ref, $wr_res);
            
            #system restart
            print $wr_res "Start $restart_count:\n";
            $restart_count++;
        }
        #parse line with resources log
        elsif (scalar(@split_log_line) == 5) {
            #parse date and time
            my @buf_time = parse_datetime($split_log_line[0], $split_log_line[1]);
            #sklip line in file, if wrong date or time format
            if (@buf_time != 4) {
                print wr_err "wrong string format in file \"$file_name\" on line $file_line\n"; 
                next;
            }
            #calculate time, passed from last restart
            my $period = @buf_time[3] - @{$last_time_restart_ref}[3];
            
            #get resource name
            my $res_name = substr $split_log_line[2], 0, length($split_log_line[2]) - 1;
            
            #check and gate time
            if ($split_log_line[3] eq "START") {
                if ($split_log_line[4] eq "STARTED") {
                    if($res_stats_ref -> {$res_name} ne "") {
                        print wr_err "resource already exist, but trying to start in file \"$file_name\" on line $file_line\n";
                        next;
                    }
                    #compare year, date and month
                    my $cmp = compare_date($last_time_restart_ref, \@buf_time);
                    if ($cmp == -1) {
                            print wr_err "Pass to much time from system restart in file \"$file_name\" on line $file_line\n";
                            next;
                    }
                    elsif ($cmp == 1) {
                        $period = $period + 24 * 3600;
                    }
                    #create record in hash for this resource and 
                    #write time in seconds pass from system restart to resource trying to start
                    if ($period < 0) { die "lines in log not sorted by time in file \"$file_name\" on line $file_line\n"; }
                    my @buf_arr = (0, $period, -1, 0);
                    $res_stats_ref -> {$res_name} = [@buf_arr];
                }
                elsif ($split_log_line[4] eq "COMPLETE") {
                    if($res_stats_ref -> {$res_name} eq "") {
                        print wr_err "resource didn't exist, but trying to start in file \"$file_name\" on line $file_line\n";
                        next;
                    }
                    my $buf_stat = $res_stats_ref -> {$res_name};
                    if (@{$buf_stat}[0] != 0) {
                        print wr_err "resource didn't started start in file \"$file_name\" on line $file_line\n";
                        next;
                    }
                    #compare year, date and month
                    my $cmp = compare_date($last_time_restart_ref, \@buf_time);
                    if ($cmp == -1) {
                            print wr_err "pass to much time from system restart in file \"$file_name\" on line $file_line\n";
                            next;
                    }
                    elsif ($cmp == 1) {
                        $period = $period + 24 * 3600;
                    }
                    #write time in seconds pass for resource start complete
                    if ($period < 0) { die "lines in log not sorted by time in file \"$file_name\" on line $file_line\n"; }
                    $res_stats_ref -> {$res_name}[0] = 1;
                    $res_stats_ref -> {$res_name}[1] = $period - ($res_stats_ref -> {$res_name}[1]);
                }
                else {
                    print wr_err "wrong format in file \"$file_name\" on line $file_line\n";
                    next;
                }
            }
            #resource stop
            elsif ($split_log_line[3] eq "STOP") {
                if ($split_log_line[4] eq "STARTED") {
                    if($res_stats_ref -> {$res_name} eq "") {
                        print wr_err "resource didn't exist, but trying to start in file \"$file_name\" on line $file_line\n";
                        next;
                    }
                    #get resource statistic from global hash
                    my $buf_stat = $res_stats_ref -> {$res_name};
                    if (@{$buf_stat}[0] != 1) {
                        print wr_err "resource didn't complete start in file \"$file_name\" on line $file_line\n";
                        next;
                    }
                    #compare year, date and month
                    my $cmp = compare_date($last_time_restart_ref, \@buf_time);
                    if ($cmp == -1) {
                            print wr_err "pass to much time from system restart in file \"$file_name\" on line $file_line\n";
                            next;
                    }
                    elsif ($cmp == 1) {
                        $period = $period + 24 * 3600;
                    }
                    #write time in seconds pass for resource stop started
                    if ($period < 0) { die "lines in log not sorted by time in file \"$file_name\" on line $file_line\n"; }
                    $res_stats_ref -> {$res_name}[2] = 0;
                    $res_stats_ref -> {$res_name}[3] = $period;
                }
                elsif ($split_log_line[4] eq "COMPLETE") {
                    if($res_stats_ref -> {$res_name} eq "") {
                        print wr_err "resource didn't exist, but trying to start in file \"$file_name\" on line $file_line\n";
                        next;
                    }
                    my $buf_stat = $res_stats_ref -> {$res_name};
                    if (@{$buf_stat}[2] != 0) {
                        print wr_err "resource didn't started stop in file \"$file_name\" on line $file_line\n";
                        next;
                    }
                    #compare year, date and month
                    my $time_stop = 0;
                    my $cmp = compare_date($last_time_restart_ref, \@buf_time);
                    if ($cmp == -1) {
                            print wr_err "pass to much time from system restart in file \"$file_name\" on line $file_line\n";
                            next;
                    }
                    elsif ($cmp == 1) {
                        $period = $period + 24 * 3600;
                    }
                    #write time in seconds pass for resource stop complete
                    if ($period < 0) { die "lines in log not sorted by time in file \"$file_name\" on line $file_line\n"; }
                    $res_stats_ref -> {$res_name}[2] = 1;
                    $res_stats_ref -> {$res_name}[3] = $period - ($res_stats_ref -> {$res_name}[3]);
                }
                else {
                    print wr_err "wrong string format in file \"$file_name\" on line $file_line\n";
                    next;
                }   
            }
            else {
                print wr_err "wrong string format in file \"$file_name\" on line $file_line\n";
                next;
            }
        }
        else {
            print wr_err "wrong line $file_line in file \"$file_name\"\n";
        }
    }
    #close all opened resources
    close $fh;
    close wr_res;
    close wr_err;
    return $restart_count;
}
1;